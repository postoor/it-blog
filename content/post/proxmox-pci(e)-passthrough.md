---
title: "Proxmox Pci(e) Passthrough"
subtitle: ""
date: 2019-09-20T10:43:59+08:00
tags: ["proxmox", "linux", "kvm", "game"]
---

PCI(e) Passthrough技術允許Host將PCI上的元件轉介給虛擬機使用，比方說可以將GPU轉給 Windows VM 使用，讓vm能玩一些比較吃顯卡的遊戲。

<!--more-->

## 環境
- OS: PROXMOX 6.0
- CPU: INTEL i5-4460

## 設定
1. 設定Host機，開啟IOMMU
```shell
vim /etc/default/grub
#### /etc/default/grub ####
GRUB_CMDLINE_LINUX_DEFAULT="quiet"
# 修改為
# INTEL
GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=on"
# AMD
GRUB_CMDLINE_LINUX_DEFAULT="quiet amd_iommu=on"
#### ================= ####

# 更新grub
update-grub
```

2. 引入需求modules

```shell
vim /etc/modules
#### /etc/modules ####
# 加入
vfio
vfio_iommu_type1
vfio_pci
vfio_virqfd
#### ============ ####
```

3. 關閉相關的顯卡驅動
```shell
echo "blacklist radeon" >> /etc/modprobe.d/blacklist.conf 
echo "blacklist nouveau" >> /etc/modprobe.d/blacklist.conf 
echo "blacklist nvidia" >> /etc/modprobe.d/blacklist.conf 
```

4. 重開機後，檢查IOMMU support和isolation
```shell
# test iommu support
shopt -s nullglob
for d in /sys/kernel/iommu_groups/*/devices/*; do 
    n=${d#*/iommu_groups/*}; n=${n%%/*}
    printf 'IOMMU Group %s ' "$n"
    lspci -nns "${d##*/}"
done;

# check iommu isolation
find /sys/kernel/iommu_groups/ -type l
```
    - `如果要加入的介面卡和其他的卡groups ID相同時，加入VM後可能會造成Host無法正常使用，此時可以試著更換卡槽，避開其他卡。`

5. 設定vm，將卡轉給VM使用
    1. 設定VM，並加入介面卡，如果需要支援PCI-E請將vm調整為q35 Machine `暫時不要勾選Primary GPU`
    2. 開啟VM安裝顯卡驅動，結束後視情況選擇是否要勾選Primary GPU，如果需要由顯卡輸出到螢幕的話需要開啟，純串流遊玩就不用了，也可以設定SPICE來共享桌面使用。