---
title: "在LXC上面使用 Forticlient VPN連線"
date: 2020-10-23T11:42:18+08:00
tags: ["Linux", "forticlient", "LXC", "openfortivpn"]
---

公司環境有使用Forti VPN分隔，目前的開發會在本地建立LXC開發機開發，再連線VPN存取DB。

雖然能透過Host機連線VPN，但連線後常常造成瀏覽其他網頁反應緩慢，所以決定在開發機中連接VPN。

<!--more-->

### Notice
` LXC 或 Docker 等 Containers 因為安全性考量，通常會禁止存取特定裝置或檔案，以下方法會開啟 Containers 特權模式，請自行考量風險後再使用 `

## 設定 LXC config

```shell
# enable privilege mode
lxc config set dev security.privileged=true

# allow device
lxc config set dev raw.lxc=lxc.cgroup.devices.allow='c 108:0 rwm'

# mount /dev/ppp
lxc config set dev raw.lxc=lxc.mount.entry='/dev/ppp dev/ppp none bind,create=file'
```

## 在containers並登入

```shell
# Install openfortivpn
sudo apt install openfortivpn


sudo mknod /dev/ppp c 108 0

# 設定檔可參考 https://github.com/adrienverge/openfortivpn#examples
sudo openfortivpn -q -c ~/.config/openfortivpn/vip0-config
```