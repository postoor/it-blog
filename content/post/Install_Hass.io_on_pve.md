---
title: "在Proxmox VE中架設 Home Assistant"
date: 2020-01-06T13:19:19+08:00
tags: ["Hass.io", "Home Assistant", "居家自動化", "Proxmox"]
---

Home Assistant是居家自動化系統，可以將未支持Google Home服務或自行開發的裝置，橋接給Google Home使用，也可以直接在Home Assistant上查看及控制裝置。

官方的Hass.io目前提供Raspberry Pi及vm安裝等方案，為了備份方便決定使用VM安裝。

<!--more-->

### 1. 下載
先到[官方網站](https://www.home-assistant.io/hassio/installation/)下載VMDK的映像檔至PVE中。

![Hass.io Image Download](/img/hass.io/hassio-download.png)

### 2. 在PVE中建立虛擬機

1. 先建立一台VM，並將原本的硬碟移除

2. 進入PVE的終端頁面，將image匯入此虛擬機
```shell
# 將vmdk轉為qcow2格式
qemu-img convert -f vmdk hassos_ova-3.7.vmdk -O qcow2 hassos_ova-3.7.qcow2

# 匯入虛擬機
# qm importdisk <qemu-id> <IMAGE> <Storage-Name>
qm importdisk 100 hassos_ova-3.7.qcow2 local-lvm

```

3. 將匯入的硬碟掛上，並調整到32G以上
![Resize Disk](/img/hass.io/qemu-resize_disk.png)

4. 設定為UEFI開機，並加入EFI Disk
![Qemu Hardware Setting](/img/hass.io/qemu-hardware.png)

5. (Option) 開啟Qemu Guset Agent，方便查看IP
![Qemu Options Config](/img/hass.io/qemu-options.png)

6. 啟動虛擬機

### 3. 設定Home Assistant
1. 取得IP後，連線到 http://<IP>:8123進行設定
2. 設定初始使用者
![HA Setting 01](/img/hass.io/HA-setting-01.png)
3. 設定安裝地址、時區及海拔
![HA Setting 02](/img/hass.io/HA-setting-02.png)
4. 加入以掃描到的裝置
![HA Setting 03](/img/hass.io/HA-setting-03.png)
5. 設定完成，可以開始使用了
![HA Setting 04](/img/hass.io/HA-setting-04.png)

### 推薦插件
- Configurator
 - 可以在網頁中開啟HA的設定檔進行調整
    ![HA Setting 04](/img/hass.io/HA-add-ons.png)
