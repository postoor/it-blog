---
title: "取得硬碟的UUID的N種方法"
date: 2019-05-10T15:25:33+08:00
tags: ["disk", "Linux"]
---

新增硬碟時，會需要自行設定`/etc/fstab`來自動掛載硬碟，此時會建議使用UUID來掛載，避免更換硬碟位置後無法正常掛載。
以下提供各類方法來取得uuid

<!--more-->

```shell
# ls
ls -l /dev/disk/by-uuid/ | grep /dev/sda1

# lsblk
lsblk -o +uuid /dev/sda1

# blkid
blkid /dev/sda1

# 懶得複製uuid時可以這樣做(X
echo UUID=$(blkid -s UUID -o value /dev/sda1 ) /media/disk  ext4  defaults  0  0 >> /etc/fstab


```