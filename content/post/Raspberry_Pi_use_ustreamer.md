---
title: "Raspberry Pi在 Ubuntu 20.04 使用Camera Module串流影像"
date: 2021-04-04T12:14:21+08:00
tags: ["mjpg", "ustreamer", "Linux", "Raspberry Pi"]
---
Raspberry Pi在 Ubuntu 20.04 使用Camera Module串流影像

<!--more-->

## 安裝 raspi-config

```shell

# 下載
wget http://mirrors.ustc.edu.cn/archive.raspberrypi.org/debian/pool/main/r/raspi-config/raspi-config_20201108_all.deb

# 安裝
sudo dpkg -i raspi-config_20201108_all.deb

# 修正相依性
sudo apt install -f
```

## 啟用相機模組

```shell
# 掛載分割區
sudo mount /dev/mmcblk0p1 /boot/

sudo raspi-config
```

啟用模組
Interface Options > P1 Camera

## 安裝 ustreamer

```shell
# 安裝相依性套件
sudo apt install -y -qq build-essential libevent-dev libjpeg-dev libbsd-dev

# clone ustreamer
git clone https://github.com/pikvm/ustreamer && cd ustreamer

# 編譯
make

# 安裝
sudo make install
```

## 啟用串流

```shell
# 啟用串流
ustreamer --device /dev/video0 --host 0.0.0.0 --resolution 1920x1080 --desired-fps 23 --encoder HW
```

## 設定service (option)

```
# 新增service檔
cat <<EOF | sudo tee /lib/systemd/system/camera.service
[Unit]
Description=Camera Service

[Service]
Type=simple
ExecStart=/usr/local/bin/ustreamer --device /dev/video0 --host 0.0.0.0 --resolution 1920x1080 --desired-fps 23 --encoder HW
Restart=on-failure
EOF

# 啟用service
sudo systemctl enable --now camera.service
```

## 旋轉相機鏡頭 (option)

```shell
# 安裝v4l2-ctl
sudo apt install v4l-utils

# 旋轉180度
v4l2-ctl --device /dev/video0 --set-ctrl rotate=180
```