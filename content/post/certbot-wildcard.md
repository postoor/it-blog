---
title: "使用 Certbot 自動更新 Wildcard 憑證"
date: 2020-09-17T09:09:14+08:00
tags: ["DNS", "certbot", "certificate", "CloudFlare"]
---

使用Certbot自動更新Wildcard SSL憑證。

<!--more-->

使用Certbot取得Wildcard憑證時，Certbot會要求你在DNS上留下特定的TXT Record，驗證你是否擁有Domain的所有權。

所以你必須先確定能修改DNS Record的紀錄，且本身使用的DNS Service有被包含在[此支援清單](https://certbot.eff.org/docs/using.html#dns-plugins)中。

以下以CloudFlare為例，取得金鑰及其他DNS Service請參考[DNS Plugins](https://certbot.eff.org/docs/using.html#dns-plugins)的說明文件。

### 系統環境
OS: Ubuntu 20.04


1. 安裝certbot及對應的dns plugins
```shell
    sudo apt install -y -qq certbot python-certbot-dns-cloudflare
```

2. 設定DNS Service API token
```shell
    sudo mkdir -p /etc/letsencrypt/.secret/

    sudo touch /etc/letsencrypt/.secret/cloudflare.ini

    sudo chmod 600 /etc/letsencrypt/.secret/cloudflare.ini

    sudo vim /etc/letsencrypt/.secret/cloudflare.ini
```

```ini
# /etc/letsencrypt/.secret/cloudflare.ini

dns_cloudflare_email=your-email@example
dns_cloudflare_api_key=<im api token>

```

3. 取得Wildcard SSL certificate
```bash
sudo certbot certonly --dns-cloudflare \
    --dns-cloudflare-credentials /etc/letsencrypt/.secret/cloudflare.ini \
    --dns-cloudflare-propagation-seconds 60 \
    -d *.404nofound.com \
    -d 404nofound.com 
```

4. 在Nginx設定中加入憑證
```conf
server {

    .......

    listen 443 ssl http2;
    server_name blog.404nofound.com;

    ssl_certificate /etc/letsencrypt/live/404nofound.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/404nofound.com/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}
```

5. 重新載入Nginx
```shell
    sudo nginx -t && sudo service nginx reload
```

6. 確認是否開啟certbot.timer
```shell
sudo systemctl status certbot.timer

# 開啟certbot timer
sudo systemctl enable certbot.timer

# 檢查是否成功啟動
sudo systemctl list-timer
```



### 參考資料
- [https://certbot.eff.org/lets-encrypt/ubuntufocal-nginx](https://certbot.eff.org/lets-encrypt/ubuntufocal-nginx)
- [https://certbot.eff.org/docs/using.html#dns-plugins](https://certbot.eff.org/docs/using.html#dns-plugins)
- [https://certbot-dns-cloudflare.readthedocs.io/en/stable/](https://certbot-dns-cloudflare.readthedocs.io/en/stable/)