---
title: "tcpdump 設定與使用"
date: 2019-03-05T10:51:35+08:00
tags: ["network", "tcpdump"]
---

tcpdump 設定與使用


<!--more-->

## 設定
### non root
```shell
sudo groupadd pcap
sudo usermod -a -G pcap $USER
sudo chgrp pcap $(type -p tcpdump)
sudo setcap cap_net_raw,cap_net_admin=eip $(type -p tcpdump)
```

## 使用
### capture filter
```shell
# 預設抓取全部封包，直接顯示
tcpdump
# -w filename /*write to file*/
# -i interface /*capture form*/
# -r filename /*read from file*/
# -n /*Don't convert addresses*/

# 只抓取port eq 80的封包
tcpdump 'port 80'

# 抓取後直接存成檔案
tcpdump -w capture.ecap

# 讀取檔案
tcpdump -r capture.ecap

# 
tcpdump -qns 0 -X -r capture.ecap
```