---
title: "Script免儲存，直接執行"
date: 2019-04-16T16:28:44+08:00
tags: ["shell", "linux"]
---

有時會從網路上直接抓script執行，通常會下載後在執行，但每次都會留下檔案忘記清除。
此時可透過以下方法直接執行。

<!--more-->

```shell
# 非互動式script
wget -O - https://github.com/non-interactive-scripts.sh | bash

# 互動式script
bash <(wget -q -o /dev/null -O - https://github.com/interactive-scripts.sh)

# 互動式script，使用root
sudo su -c "bash <(wget -q -o /dev/null -O - https://github.com/interactive-scripts.sh)" root
```