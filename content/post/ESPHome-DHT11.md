---
title: "在Home Assistant中，使用ESPHome製作溫濕度感應器"
date: 2020-01-06T14:47:33+08:00
tags: ["Home Assistant", "ESPHome", "ESP8266", "ESP-01", "居家自動化"]
---
ESPHome是一個能提供你由簡易設定檔來控制ESP8266/ESP32的系統，可以與Home Assistant串連來進行讀取和控制。

<!--more-->
### 硬體
- ESP-01
<img src="/img/esphome/ESP-01.jpg" width="40%" alt="ESP-01">
<!-- ![ESP-01](/img/esphome/ESP-01.jpg) -->
- DHT11 Modules
<img src="/img/esphome/ESP-01_DHT11_modules.png" width="40%" alt="DHT11 Modules">
<!-- ![DHT11 Modules](/img/esphome/ESP-01_DHT11_modules.png) -->

### 前置作業
1. 安裝`esptool.py`
    - 執行`pip install esptool`或依照[https://github.com/espressif/esptool](https://github.com/espressif/esptool)的方式手動安裝
2. 在Home Assistant安裝 ESPHome add-ons
    - 方法請參考[ESPHome官網](https://esphome.io/guides/getting_started_hassio.html)

### 建立裝置
1. 先至ESPHome建立一個裝置
2. 在新增的裝置上點選修改撰寫設定檔
    - 以ESP-01為例填入下列設定就可使用基本的溫濕度讀取，也可參考我的[Github](https://github.com/postoor/ESPHome-Project/blob/master/ESP-01_with_DHT11_modules/esp_dht11.yaml)範例
```yaml
esphome:
  name: esp_dht11
  platform: ESP8266
  board: esp01_1m

wifi:
  ssid: "WiFi-SSID"
  password: "WiFI-Passwd"

sensor:
  - platform: dht
    model: DHT11
    pin: 2
    temperature:
      name: "Living Room Temperature"
    humidity:
      name: "Living Room Humidity"
    update_interval: 10s
```
3. 儲存後點選Compile，並下載.bin檔
4. 使用`esptool.py`將bin刷入ESP-01
    - 使用USB to UART接線方式可參考[https://atceiling.blogspot.com/2019/06/arduino33-esp8266-dht-11.html](https://atceiling.blogspot.com/2019/06/arduino33-esp8266-dht-11.html)
    - 嫌麻煩可以使用此轉接板[https://goods.ruten.com.tw/item/show?21830392593421](https://goods.ruten.com.tw/item/show?21830392593421)
```shell
sudo esptool.py --port /dev/ttyUSB0 write_flash 0x0000 lamp_switch.bin
```

5. 接上DHT11 modules後，可在ESPHome上查看是否上線
![ESPHome Page](/img/esphome/ESPHome-device-01.png)
6. 到 `設定 > 整合`中新增ESPHome設備
7. 新增成功後可以到總覽頁查看是否有加入設備，如果沒有可以自行加入
![HA Dashboard](/img/esphome/HA-dashboard.png)


### Troubleshooting

1. ESPHome使用WiFI連線時，預設會會使用`<裝置名稱>.local`，但是我這會一直抓不到裝置，所以後來改用固定IP連線
```yaml
wifi:
    ssid: "WiFi-SSID"
    password: "WiFi-Password"
    manual_ip:
        static_ip: 192.168.0.20
        gateway: 192.168.0.1
        subnet: 255.255.255.0
```


2. DHT11 modules這顆模組我在使用上時有遇到幾個問題
    1. 接上模組時ESP-01會無法使用WiFi連線，但將pin腳拉出來在麵包板上測試時又可以正常使用
        - 實測好像是模組本身的設計有點問題，WiFI天線會被干擾到，後來在ESP-01的背後貼上鋁箔後有比較改善，但是還是訊號還是比其他片微弱(-5x dB vs -18 dB) = =
        <img src="/img/esphome/ESP-01_Bug.jpg" width="40%" alt="ESP-01 bug">
        <!-- ![ESP-01 bug](/img/esphome/ESP-01_Bug.jpg) -->
    2. 從HA的紀錄上可以看到，有時候溫濕度的紀錄會失靈
        ![Bug Temp](/img/esphome/Bug_temp.png)
        ![Bug Hum](/img/esphome/Bug_hum.png)
        - 感覺是DHT11的模組有點問題，之後再買DHT11或DHT12重新焊上試試，我買的兩顆不知道是不是賣家在焊時沒弄好，模組的外殼都變形了....
        ![static/img/esphome/ESP-01_with_DHT11_modules.jpg] (/img/esphome/ESP-01_with_DHT11_modules.jpg)