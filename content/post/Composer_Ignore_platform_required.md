---
title: "Composer 安裝套件時，忽略平台相依性檢查"
date: 2021-03-11T14:08:32+08:00
tags: ["composer","PHP", "PHP8"]
---

試著使用PHP 8架設環境時，遇到相依性套件僅支援PHP 7.x版的問題。

<!--more-->


```shell
$ composer install --no-dev
Installing dependencies from lock file
Verifying lock file contents can be installed on current platform.
Your lock file does not contain a compatible set of packages. Please run composer update.

  Problem 1
    - illuminate/container is locked to version v5.8.36 and an update of this package was not requested.
    - illuminate/container v5.8.36 requires php ^7.1.3 -> your php version (8.0.3) does not satisfy that requirement.
  Problem 2
    - illuminate/contracts is locked to version v5.8.36 and an update of this package was not requested.
    - illuminate/contracts v5.8.36 requires php ^7.1.3 -> your php version (8.0.3) does not satisfy that requirement.

```

查了一下發現Composer 2有加入`--ignore-platform-req`的參數，提供忽略PHP版差和`ext-`相關套件。

```shell
# 忽略PHP版差
composer install --ignore-platform-req=php 

# 忽略ext-zip
composer install --ignore-platform-req=ext-zip

```
