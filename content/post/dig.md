---
title: "Dig 常用指令"
date: 2019-05-10T13:41:16+08:00
tags: ["shell", "linux", "dns"]
---

搬遷name servers 或 DNS有問題時，常用到dig來查詢設定是否更新。

<!--more-->

```shell
# 查詢Name server
dig NS 404nofound.com.

# 查詢特定DNS server
dig NS @8.8.8.8 404nofound.com.
dig NS @1.1.1.1 404nofound.com.

# 查詢A Record
dig a blog.404nofound.com.

# 查詢CNAME
dig CNAME blog.404nofound.com.
```