---
title: "Install oh My Zsh on Ubuntu"
date: 2020-06-02T13:05:35+08:00
draft: false
tags: ["zsh", "linux", "oh-my-zsh"]
---
在Ubuntu中使用oh-my-zsh

<!--more-->

### 1. 安裝相依性套件

```shell
sudo apt install -y -qq zsh zsh-autosuggestions git curl
```

### 2. 安裝字型

```shell
sudo apt install -y -qq fonts-powerline fonts-firacode
```

### 3. 安裝oh-my-zsh

```shell
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

### 4. (Optional) 設定zsh theme

```shell
sed 's/^ZSH_THEME="\w.*/ZSH_THEME="agnoster"/' ~/.zshrc > ~/.zshrc
```
