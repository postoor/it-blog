---
title: "Laravel在CentOS，無法寫入Log的問題"
date: 2020-11-04T11:32:42+08:00
tags: []
---

今天在搬遷環境時，遇到目錄權限全開放也無法寫入Log的問題。

測試過後才發現是SELinux `httpd_sys_rw_content_t`限制了php-fpm的存取。

<!--more-->

## 開啟內容存取權限
```shell
sudo chcon -R -t httpd_sys_rw_content_t storage
```

## 無法連接Redis
```shell
sudo setsebool httpd_can_network_connect=1
```
