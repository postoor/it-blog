---
title: ".gitignore 使用"
date: 2019-03-11T09:07:33+08:00
tags: ["git", "gitignore"]
---

有時專案內會有一些由程式產生或者不想放進git中的私密資料，這時可以利用 `.gitignore` 的檔案來忽略他們。 

<!--more-->

## 使用
```shell
$~: vim .gitignore

# 忽略特定檔案
.env
config/database.php

# 忽略特定目錄
logs/

# 忽略特定類型的檔案
*.toml

```

## 特殊使用
有時候會需要保留被忽略的特定檔案，這時可以使用`git add -f`來加入

```shell
# 強制加入特定檔案
git add -f config/param.php

# 如要忽略資料夾，但要保留目錄結構的話可以這樣做
touch public/.gitkeeper && git add -f public/.gitkeeper

```

## Troubleshooting
當要忽略的目錄已經有檔案，且已經進追蹤時，即使已忽略此目錄，每次修改時還是會顯示modified。
此時可以先移除要排除的檔案，重新commit後，gitignore的設定即可正常運作

```shell
rm -rf public/* 
git add -A public/
```