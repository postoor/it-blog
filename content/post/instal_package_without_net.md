---
title: "Ubuntu在無網路的情況下安裝套件"
date: 2019-08-23T08:49:10+08:00
tags: ["Ubuntu", "Linux"]
---

有些客戶的環境嚴格限制電腦連上外部網路，也不一定會提供內部的鏡像站，
所以有時候只能自己下載所需要的套件及相依套件來替他佈署環境。

<!--more-->

### 系統環境
OS: Ubuntu 16.04

### 下載套件
```shell

# 使用Docker運行Ubuntu 16.04來下載套件 (option)
docker run --rm -it --user $UID -v  $PWD/:/package ubuntu:16.04
# In Docker
cd /package

function downPkg() {
apt-get download $(apt-cache depends --recurse --no-recommends  --no-suggests \
  --no-conflicts --no-breaks --no-replaces --no-enhances \
  --no-pre-depends $1 | grep "^\w")
}
downPkg "nginx"
downPkg "vim curl"
exit
```

### 安裝
```shell
dpkg -i /package/*.deb
```
