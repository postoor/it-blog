---
title: "使用Systemd Timer開關Gnome通知"
date: 2024-02-15T05:04:59Z
tags: ["Linux", "Systemd"]
---
紀錄一下Systemd User timer的使用方式

<!--more-->

設定使用者自訂的timer & service

### 設定

使用者設定的預設路徑 `~/.config/systemd/user/`
可使用`{service_name}@{%i}.timer` & `{service_name}@.service`方式來傳遞`%i`給service作為參數使用

以下以開關Gnome的Disturb為例

設定內容
```shell
# ~/.config/systemd/user/disturb@false.timer
[Unit]
Description=Timer to run disable Disturb

[Timer]
# format    ┌ Day of week: Mon,Fri (1&5) / Mon (1 only) / Mon..Fri (1~5)
#           |       ┌ YYYY-mm-dd HH:ii:ss
#           |       |
OnCalendar=Mon..Fri *-*-* 23:00:00

[Install]
WantedBy=timers.target


# ~/.config/systemd/user/disturb@.service
[Unit]
Description=Set Disturb: %i

[Service]
ExecStart=gsettings set org.gnome.desktop.notifications show-banners %i

[Install]
WantedBy=multi-user.target

```

啟用/查看執行狀態
```shell
# reload config
systemctl --user daemon-reload

# enable
systemctl --user enable --now disturb@false.timer

# list timer
systemctl --user list-timers

# service status
systemctl --user status disturb@false.service
```

檢查工具
```shell
# check calendar format
systemd-analyze calendar "Mon..Fri *-*-* 02:16:00"
```


### 參考資料
- [systemd.time(7) - Linux manual page](https://www.man7.org/linux/man-pages/man7/systemd.time.7.html#CALENDAR_EVENTS)
- [systemd/User - ArchWiki](https://wiki.archlinux.org/title/systemd/User)
- [https://www.reddit.com/r/systemd/comments/qwmwwg/single_service_unit_file_with_different_timers/?rdt=59521](https://www.reddit.com/r/systemd/comments/qwmwwg/single_service_unit_file_with_different_timers/?rdt=59521)
