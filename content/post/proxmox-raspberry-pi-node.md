---
title: "為Proxmox Cluster新增Raspberry pi節點"
date: 2019-06-09T20:21:08+08:00
tags: ["proxmox", "pve", "Linux", "Raspberry Pi"]
---

因為手上只有兩台空的x86 PC可以組Proxmox，萬一有其中一台Proxmox停機或失連時，剩下的Proxmox將不能新增vm。
在此可以將raspberry pi加入叢集內參與投票，避免一台Proxmox停機時無法使用

<!--more-->

*** 注意 ***
此方法不建議使用在正式環境，且此方法新增的rpi無法在Web上查看他的上線情況及投票狀況。


### 建立Raspberry pi node
本次使用 Raspbian stretch，開機後更新套件庫並安裝corosync
* 以下使用root權限執行
```shell
apt update && apt install corosync

sed -i 's/without-password/yes/' /etc/ssh/sshd_config && systemctl restart ssh
```

修改/etc/hosts將proxmox的hosts與IP加入
```shell
nano /etc/hosts

# 可使用raspi-config修改raspberry pi hosts name
127.0.1.1 pve-rpi
10.0.0.2 pve-1
10.0.0.3 pve-2
```
複製現有node上的相關資料
```shell
scp root@10.0.0.2:/etc/corosync/* /etc/corosync/
```

新增rpi進nodelist
```shell
nano /etc/corosync/corosync.conf

nodelist {
  ............
  node {
    name: pve-rpi
    nodeid: 3
    quorun_votes: 1
    ring0_addr: 10.0.0.4
  }
}

```

先重起proxmox corosync，再啟用rpi的corosync
```shell

# proxmox node
systemctl restart corosync

# rpi
systemctl enable corosync
systemctl start corosync
```

檢查上線狀況
```shell 
corosync-quorumtool



Quorum information
------------------
Date:             Sun Jun  9 14:03:47 2019
Quorum provider:  corosync_votequorum
Nodes:            3
Node ID:          3
Ring ID:          3/231136
Quorate:          Yes

Votequorum information
----------------------
Expected votes:   3
Highest expected: 3
Total votes:      3
Quorum:           2  
Flags:            Quorate 

Membership information
----------------------
    Nodeid      Votes Name
         3          1 10.0.0.4 (local)
         1          1 10.0.0.2
         2          1 10.0.0.3

```

Web GUI顯示狀況
![Web GUI 01](/img/proxmox-rpi/rpi-node-1.png)
![Web GUI 02](/img/proxmox-rpi/rpi-node-2.png)