---
title: "Firefox 無法正常顯示 Proxmox Xterm.js 內的文字"
date: 2021-04-13T09:09:18+08:00
tags: ["proxmox", "linux", "xterm.js", "firefox"]
---

Firefox 如果有開啟 `privacy.resistFingerprinting: true`,將無法正常顯示 `Xterm.js` 內的文字。

<!--more-->

## 解法

> about:configs > privacy.resistFingerprinting => false


## 資料來源
[Firefox doesn't show the texts of xtermjs](https://support.cdn.mozilla.net/zh-CN/questions/1327130)