---
title: "Install Kafka"
date: 2019-08-13T09:51:55+08:00
tags: ["kafka", "Linux"]
---

Kafka是一種分散式訊息系統，使用scala撰寫，擁有高吞吐量和水平擴展性。
以下紀錄安裝及設定service的過程

<!--more-->

### 系統環境
- OS: Ubuntu 18.04
- RAM: 官方建議至少`4GB`

### 安裝
1. openJDK

```shell
sudo apt install openjdk-8-jdk
```

2. kafka

```shell

sudo adduser kafka # (option)
sudo su kafka # (option)

mkdir kafka && cd kafka && \
wget http://apache.stu.edu.tw/kafka/2.3.0/kafka_2.11-2.3.0.tgz
tar -xvzf kafka_2.11-2.3.0.tgz --strip 1
```

3. zookeeper.service

```shell
sudo vim /lib/systemd/system/zookeeper.service

# 以下為zookeeper.service內容
[Unit]
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
ExecStart=/home/kafka/kafka/bin/zookeeper-server-start.sh /home/kafka/kafka/config/zookeeper.properties
ExecStop=/home/kafka/kafka/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target

```

4. kafka.service

```shell
sudo vim /lib/systemd/system/kafka.service

# 以下為kafka.service內容
[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=simple
User=kafka
ExecStart=/bin/sh -c '/home/kafka/kafka/bin/kafka-server-start.sh /home/kafka/kafka/config/server.properties > /home/kafka/kafka/kafka.log 2>&1'
ExecStop=/home/kafka/kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```


5. 啟用service

```shell
sudo systemctl enable zookeeper.service
sudo systemctl enable kafka.service

sudo service zookeeper status
sudo service kafka start
```


### Note
- kafka本身會由zookeeper控管，請確認zookeeper已經啟動才能正常啟動kafka

- 設定調整部份請至kafka/config中設定
    - kafka: config/server.properties
    - zookeeper: config/zookeeper.properties