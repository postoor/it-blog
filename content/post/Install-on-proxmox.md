---
title: "Proxmox 安裝 Gnome"
date: 2019-11-05T15:01:02+08:00
draft: false
tags: ["proxmox", "linux", "gnome"]
---

Proxmox 使用Debian 為基底，所以可以將PVE裝上Gnome等桌面環境來作為一般桌機使用。

<!--more-->

## 安裝
1. 更換 Proxmox VE No-Subscription Repository & 更新系統

```shell

# 將enterprise ppa 移出套件庫
mv /etc/apt/sources.list.d/pve-enterprise.list /etc/apt/sources.list.d/pve-enterprise.list.bk
# 將 non-enterprise ppa 加入套件庫
echo "deb http://download.proxmox.com/debian/pve buster pve-no-subscription" > /etc/apt/sources.list.d/pve-no-enterprise.list

# 更新套件清單及系統
apt update -qq &&  apt upgrade -y -qq

# 重開機
reboot

```

2. 安裝桌面及相關套件 & 新增桌面使用者

```shell
#  安裝sudo等工具 (Option)
apt install -y -qq sudo tmux vim htop

# 安裝Gnome桌面環境
tasksel install desktop gnome-desktop

#  移除 network-manager (Option)
apt remove network-manager-gnome  network-manager

# 啟用桌面環境
systemctl set-default graphical.target

# 加入使用者
adduser <USERNAME>

# 加入sudo群組(option)
usermod -a -G sudo <USERNAME> 

# 重開機
reboot
```

