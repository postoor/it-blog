---
title: "Ubuntu移除新細明體和標楷體"
date: 2020-03-31T09:28:17+08:00
tags: ["Ubuntu", "Linux"]
---

用Firefox瀏覽一些網站時，英文字體兩邊常出現黑邊，
可以直接移除`fonts-arphic-ukai fonts-arphic-uming`兩種字型來避免網站使用這兩種字型。
<!--more-->

```shell
    sudo apt-get remove fonts-arphic-ukai fonts-arphic-uming
```

移除前
![have font](/img/fonts/have_fonts.png)

移除後
![no font](/img/fonts/none_fonts.png)
