---
title: "從Xenserver轉移到Proxmox"
date: 2019-05-10T16:03:47+08:00
tags: ["proxmox", "pve", "Linux"]
---

最近要更換測試機，決定從Xen轉到pve使用。
因為是DB，考量到重新建置要花的時間~~以及我的惰性~~，所以決定匯出虛擬機到pve使用。

<!--more-->

## 準備工作
- 下載工具: [xenmigrate.py](https://github.com/derekjhyang/xenserver_to_xen)

**`注意`**:
- 硬碟空間，過程中會需要轉出兩次，請確保至少有映像檔三倍以上的空間


## 1. 由xen匯出xva檔

```shell
# 列出所有vm,可從此查看uuid
xe vm-list

# (optional) 可做snapshot再由snapshot匯出xva
xe vm-snapshot new-name-label="snapshot for export" vm={vm uuid}

# 匯出完整映像檔
xe vm-export vm={vmOrSnapshotUUID} filename={path/xva-name.xva}
```

## 2. 轉換映像檔格式

先將匯出的xva檔丟到pve上

```shell
# 解壓縮xva
tar -xf xva-name.xva

# 查看有哪些Ref開頭的資料夾
ls Ref*

# 轉換成raw image
python xenmigrate.py -c Ref:123 vm_raw_0.img

# 需要的話再轉成qcow2
qemu-img convert -f raw -O qcow2 vm_raw_0.img vm_0.qcow2
```

## 3. 建立vm

在pve上面依原有的設定開一台虛擬機,再用轉出的qcow2覆蓋掉原有的qcow2


## Troubleshooting

1. 我有兩顆硬碟，開機後抓不到其中一顆
    - 先檢查/etc/fstab和lsblk對不對的起來，通常有87%是拿硬碟分割區名稱來掛載導致的，請改用UUID

2. 網路起不來
   - 檢查ip addr 和 /etc/network/interfaces 對不對的起來，對不起來請更換為新的網卡名稱
   - 如ip addr沒有網卡，試著到pve更換網卡Model
