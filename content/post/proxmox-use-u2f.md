---
title: "Proxmox 使用 U2F 作二次登入驗證"
date: 2020-06-18T08:30:10+08:00
tags: ["proxmox", "U2F", "pve",]
---

U2F(Universal 2nd Factor, 通用第二因素)，是一種開放的身份驗證標準，是使用專用的實體裝置來加強並簡化雙因素驗證。

在Proxmox的管理界面啟用U2F，可以確保使用者必須同時擁有密碼及金鑰時才能登入。

<!--more-->

## 設定U2F AppID URL
1. 開啟 Datacenter -> Options 修改 U2F Settings
![Proxmox Open U2F Settings](/img/proxmox-u2f/pve-u2f-appid.png)

2. 在U2F AppID URL填入 "https://\<your-ip\>:8006"
![Proxmox edit U2F AppID URL](/img/proxmox-u2f/pve-u2f-edit-appid.png)

## 替使用者新增實體金鑰
1. 開啟 Datacenter -> Permissions -> Users 選擇要添加實體金鑰的使用者，並點選TFA
![Proxmox Users Management](/img/proxmox-u2f/pve-user-manager.png)

2. 點選U2F後按下Register U2F Device，並輕觸實體金鑰(如果需要的話)
![Proxmox User Add U2F](/img/proxmox-u2f/pve-user-tfa.png)

## 登入
![Proxmox U2F Login](/img/proxmox-u2f/pve-u2f-login.png)